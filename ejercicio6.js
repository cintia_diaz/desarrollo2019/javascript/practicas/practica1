let dni = ''; // dni introducido por el usuario
let letra= ''; // letra del dni introducida por el usuario
let letraMayus=''; // letra introducida convertida a mayúsculas
let letraCalcu=''; // letra calculada a partir del array de letras
let letras=['T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E','T'];
let resto=0; // resto de la división entera del número de DNI y 23


dni=Number(prompt('Introduce tu dni'));
letra=prompt('Introduce la letra');
letraMayus=letra.toUpperCase(); // convierto la letra introducida a mayúsculas

/*
	Si el número es menor que 0 o mayor que 99999999, el número no es válido y acaba el programa
	Si no, se calcula la letra a partir del resto y se compara con la letra indicada por el usuario
 */
if (dni<0 || dni>99999999) {
	document.write('El numero proporcionado no es valido');
} else {
	resto=dni % 23;
	letraCalcu=letras[resto];
	if (letraCalcu==letraMayus) {
		document.write('El número y la letra de DNI son correctos');
	} else {
		document.write('La letra indicada no es correcta');
	}
}
